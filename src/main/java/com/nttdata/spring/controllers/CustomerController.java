package com.nttdata.spring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nttdata.spring.repository.Customer;
import com.nttdata.spring.services.CustomerManagementServiceI;

/**
 * Controlador del cliente
 * @author Aaron
 *
 */

@RestController
@RequestMapping("/*")
public class CustomerController {
	
	@Autowired
	private CustomerManagementServiceI customerService;
	
	@GetMapping(path = "/index")
	public @ResponseBody String listCustomers() {
		List<Customer> customerList = customerService.searchAll();
		
		return "Exito al buscar todos los clientes";
	}
	
	@GetMapping(path = "/search")
	public @ResponseBody String getCustomerById(@RequestParam int id) {
		Customer customer = customerService.findById(id);
		System.out.println("Cliente encontrado\t" + customer.toString());
		
		return "Exito al buscar cliente por id";
	}
	
	@PutMapping(path = "/add")
	public @ResponseBody String insertNewCustomer(@ModelAttribute Customer newCustomer) {
		customerService.insertNewCustomer(newCustomer);
		
		return "Exito al añadir cliente";
	}
	
}
