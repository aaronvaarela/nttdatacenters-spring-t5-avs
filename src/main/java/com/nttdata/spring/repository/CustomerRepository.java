package com.nttdata.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	public List<Customer> findAll();
	public Customer findById(int id);
	public List<Customer> findAllByNameOrSurName(final String name, final String surname);
	public List<Customer> findAllByAgeGreaterThan(final Integer age);
	public List<Customer> findByNameAndAgeGreaterThan(final String name, final Integer age);
	public List<Customer> findByNameAndAge(final String name, final Integer age);
	
}
