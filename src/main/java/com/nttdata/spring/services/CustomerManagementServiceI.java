package com.nttdata.spring.services;

import java.util.List;

import com.fasterxml.jackson.core.sym.Name;
import com.nttdata.spring.repository.Customer;

public interface CustomerManagementServiceI {
	/**
	 * Operación de inserción en la base de datos
	 * @param customer {@link Customer} cliente
	 * @return {@link Customer} creado
	 */
	public Customer insertNewCustomer(Customer customer);
	
	/**
	 * Buscar un cliente en la base de datos mediante el nombre o los apellidos
	 * @param name {@link Name} nombre
	 */
	public List<Customer> searchByName(String name);
	
	/**
	 * Buscar clientes cuyo edad supere los 19 años
	 * @return {@link List<Customer>} lista de clientes
	 */
	public List<Customer> searchCustomerGreater19yo();
	
	/**
	 * Buscar un cliente en la base de datos mediante el nombre y con una edad superior a la indicada
	 * @param name {@link Name} nombre
	 * @param age {@link Age} edad
	 * @return {@link List<Customer>} lista de clientes
	 */
	public List<Customer> searchCustomerGreaterAndName(String name, Integer age);

	
	/**
	 * Buscar un cliente en la base de datos mediante el nombre y con la edad indicada
	 * @param name {@link Name} nombre
	 * @param age {@link Age} edad
	 * @return {@link List<Customer>} lista de clientes
	 */
	public List<Customer> searchCustomerByNameAndAge(String name, Integer age);
	
	
	/**
	 * Buscar todos los clientes presentes en la base de datos
	 * @return {@link List<Customer>} lista de clientes
	 */
	public List<Customer> searchAll();
	
	public Customer findById(int id);
	
	public void deleteById(int id);
	
}
