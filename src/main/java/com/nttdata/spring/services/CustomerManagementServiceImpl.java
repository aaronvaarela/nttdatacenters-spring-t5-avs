package com.nttdata.spring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.nttdata.spring.repository.Customer;
import com.nttdata.spring.repository.CustomerRepository;

@Service
@Primary
public class CustomerManagementServiceImpl implements CustomerManagementServiceI {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public Customer insertNewCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public List<Customer> searchByName(String name) {
		return customerRepository.findAllByNameOrSurName(name, name);
	}

	@Override
	public List<Customer> searchCustomerGreater19yo() {
		return customerRepository.findAllByAgeGreaterThan(19);
	}

	@Override
	public List<Customer> searchCustomerGreaterAndName(String name, Integer age) {
		return customerRepository.findByNameAndAgeGreaterThan(name, age);
	}

	@Override
	public List<Customer> searchCustomerByNameAndAge(String name, Integer age) {
		return customerRepository.findByNameAndAge(name, age);
	}

	@Override
	public List<Customer> searchAll() {
		return customerRepository.findAll();
	}
	
	@Override
	public Customer findById(int id) {
		return customerRepository.findById(id);
	}

	@Override
	public void deleteById(int id) {
		customerRepository.deleteById(id);
	}

}
