package com.nttdata.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.nttdata.spring.controllers.CustomerController;
import com.nttdata.spring.repository.Customer;
import com.nttdata.spring.services.CustomerManagementServiceI;

@SpringBootApplication
public class NttdatacentersSpringT5AvsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(NttdatacentersSpringT5AvsApplication.class, args);
		// ctx.close();
	}

	@Autowired
	private CustomerManagementServiceI customerService;

	@Autowired
	private CustomerController customerController;

	@Override
	public void run(String... args) throws Exception {

		Customer customer1 = new Customer();
		customer1.setName("Antonio");
		customer1.setSurName("Perez");
		customer1.setAge(42);

		Customer customer2 = new Customer();
		customer2.setName("Antonio");
		customer2.setSurName("Navas");
		customer2.setAge(21);

		Customer customer3 = new Customer();
		customer3.setName("Jose");
		customer3.setSurName("Gomez");
		customer3.setAge(25);

		customerService.insertNewCustomer(customer1);
		customerService.insertNewCustomer(customer2);
		
		
	}

}
